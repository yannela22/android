package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Carro;
import com.example.castroyannela.vistas.adapter.ArtistaAdapter;
import com.example.castroyannela.vistas.adapter.CarroAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActividadCarroORM extends AppCompatActivity implements View.OnClickListener {

    EditText cajaPlaca, cajaModelo, cajaMarca, cajaAnio;
    ImageButton botonGuardar, botonModificar, botonEliminar, botonEliminarTodo, botonListar, botonBuscar;
    TextView datos;
    RecyclerView recyclerCarro;
    CarroAdapter adapter;
    List<Carro> listaCarro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_carro_orm);
        cargarComponentes();
    }

    private void cargarComponentes(){
        cajaPlaca = findViewById(R.id.txtPlacaCarro);
        cajaModelo = findViewById(R.id.txtModeloCarro);
        cajaMarca = findViewById(R.id.txtMarcaCarro);
        cajaAnio = findViewById(R.id.txtAnioCarro);
        botonGuardar = findViewById(R.id.btnAgregrarCarro);
        botonModificar = findViewById(R.id.btnModificarCarro);
        botonEliminar = findViewById(R.id.btnElinimarCarro);
        botonEliminarTodo = findViewById(R.id.btnEliminarTodo);
        botonListar = findViewById(R.id.btnListarCarros);
        botonBuscar = findViewById(R.id.btnBuscarporPlaca);
        recyclerCarro = findViewById(R.id.recyclerCarroORM);
        datos = findViewById(R.id.lblORM);
        botonGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonEliminarTodo.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAgregrarCarro:
                Carro carro = new Carro();
                carro.setPlaca(cajaPlaca.getText().toString());
                carro.setModelo(cajaModelo.getText().toString());
                carro.setMarca(cajaMarca.getText().toString());
                carro.setAnio(Integer.parseInt(cajaAnio.getText().toString()));
                carro.save();
                Toast.makeText(ActividadCarroORM.this, "Guardado Correctamente", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnModificarCarro:
                try {
                    String valor = cajaPlaca.getText().toString();
                    Carro c = new Carro();
                    c = Carro.getCarsbyPlate(valor);
                    c.setModelo(cajaModelo.getText().toString());
                    c.setMarca(cajaMarca.getText().toString());
                    c.setAnio(Integer.parseInt(cajaAnio.getText().toString()));
                    c.save();
                    Toast.makeText(ActividadCarroORM.this, "Modificado Correctamente", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("Error", "error de modificar" + ex.getMessage());
                }
                break;
            case R.id.btnElinimarCarro:
                String dato = cajaPlaca.getText().toString();
                Carro car = new Carro();
                car = Carro.getCarsbyPlate(dato);
                car.delete();
                Toast.makeText(ActividadCarroORM.this, "Eliminado Correctamente", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnEliminarTodo:
                Carro.deleteAll();
                Toast.makeText(ActividadCarroORM.this, "Se eliminaron todos los carros", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnListarCarros:
                listaCarro = Carro.getAllCars();
                recyclerCarro.setLayoutManager(new LinearLayoutManager(this));
                adapter = new CarroAdapter(listaCarro);
                adapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cajaPlaca.setText(""+listaCarro.get(recyclerCarro.getChildAdapterPosition(v)).getPlaca());
                        cajaModelo.setText(""+listaCarro.get(recyclerCarro.getChildAdapterPosition(v)).getModelo());
                        cajaMarca.setText(""+listaCarro.get(recyclerCarro.getChildAdapterPosition(v)).getMarca());
                        cajaAnio.setText(""+listaCarro.get(recyclerCarro.getChildAdapterPosition(v)).getAnio());
                    }
                });
                recyclerCarro.setAdapter(adapter);
                break;
            case R.id.btnBuscarporPlaca:
                listaCarro = new ArrayList<Carro>();
                String data = cajaPlaca.getText().toString();
                Carro ca = new Carro();
                ca = Carro.getCarsbyPlate(data);
                listaCarro.add(ca);
                adapter = new CarroAdapter(listaCarro);
                recyclerCarro.setLayoutManager(new LinearLayoutManager(this));
                recyclerCarro.setAdapter(adapter);
                break;
        }
    }
}
