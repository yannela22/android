package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.castroyannela.R;

public class ActividadSensores extends AppCompatActivity implements SensorEventListener {

    SensorManager manager;
    Sensor sensor;
    TextView datoX, datoY, datoZ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensores);
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        cargarComponentes();
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this, sensor, manager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float a, b, c;
        a = event.values[0];
        b = event.values[1];
        c = event.values[2];

        datoX.setText(a+"");
        datoY.setText(b+"");
        datoZ.setText(c+"");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void cargarComponentes(){
        datoX = findViewById(R.id.labelXAcelerometro);
        datoY = findViewById(R.id.labelYAcelerometro);
        datoZ = findViewById(R.id.labelZAcelerometro);
    }
}
