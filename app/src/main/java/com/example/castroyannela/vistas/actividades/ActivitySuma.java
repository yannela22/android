package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.castroyannela.R;

public class    ActivitySuma extends AppCompatActivity implements View.OnClickListener{

    EditText n1, n2, resultado;
    Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suma);
        cargarComponentes();
    }

    private void cargarComponentes() {
        n1 = findViewById(R.id.txtN1);
        n2 = findViewById(R.id.txtN2);
        resultado = findViewById(R.id.txtResultado);
        boton = findViewById(R.id.btnCalcular);
        boton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int valor1 = Integer.parseInt(n1.getText().toString());
        int valor2 = Integer.parseInt(n2.getText().toString());
        int suma = valor1 + valor2;
        resultado.setText(suma + "");
    }
}
