package com.example.castroyannela.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Carro;

import java.util.List;

public class CarroAdapter extends RecyclerView.Adapter<CarroAdapter.ViewHolderCarro> implements View.OnClickListener {

    List<Carro> listaCarro;
    private View.OnClickListener clickListener;

    public CarroAdapter(List<Carro> carro){ this.listaCarro = carro; }

    @NonNull
    @Override
    public CarroAdapter.ViewHolderCarro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carro,null);
        view.setOnClickListener(this);
        return new ViewHolderCarro(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCarro holder, int pos) {
        holder.placa.setText(listaCarro.get(pos).getPlaca());
        holder.modelo.setText(listaCarro.get(pos).getModelo());
        holder.marca.setText(listaCarro.get(pos).getMarca());
        holder.anio.setText(""+listaCarro.get(pos).getAnio());
    }

    @Override
    public int getItemCount() {
        return listaCarro.size();
    }

    public void setOnClickListener(View.OnClickListener listener){ this.clickListener = listener;}

    @Override
    public void onClick(View v) {
        if (clickListener != null){
            clickListener.onClick(v);
        }
    }

    public static class ViewHolderCarro extends RecyclerView.ViewHolder{
        TextView placa, modelo, marca, anio;
        public ViewHolderCarro(View itemView) {
            super(itemView);
            placa = itemView.findViewById(R.id.lblPlaca);
            modelo = itemView.findViewById(R.id.lblModelo);
            marca = itemView.findViewById(R.id.lblMarca);
            anio = itemView.findViewById(R.id.lblAnio);
        }
    }
}
