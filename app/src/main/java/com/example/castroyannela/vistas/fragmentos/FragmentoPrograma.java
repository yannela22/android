package com.example.castroyannela.vistas.fragmentos;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Artista;
import com.example.castroyannela.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentoPrograma.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentoPrograma#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoPrograma extends Fragment implements View.OnClickListener{
    Button botonLeer;
    TextView dato;
    RecyclerView recyclerMP;
    InputStream input;
    ArtistaAdapter adapter;
    List<Artista> ListArtista;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentoPrograma() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentoPrograma.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentoPrograma newInstance(String param1, String param2) {
        FragmentoPrograma fragment = new FragmentoPrograma();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_actividad_memoria_programa, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        try{
            ListArtista = new ArrayList<Artista>();
            BufferedReader lector = new BufferedReader(new InputStreamReader(input));
            String listaartista = lector.readLine();
            String[] data = listaartista.split(";");
            String[] data2;
            for (int i=0; i < data.length; i++){
                data2 = data[i].split(",");
                Artista a = new Artista();
                a.setNombres(data2[0]);
                a.setApellidos(data2[1]);
                a.setNombreArtistico(data2[2]);
                a.setImagen(Uri.parse(data2[3]).toString());
                //vamos añadir los datos a  la lista
                ListArtista.add(a);
            }
            //dato.setText(listaartista);
            adapter = new ArtistaAdapter(ListArtista);
            recyclerMP.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cargarDialogo(v);
                }
            });
            recyclerMP.setAdapter(adapter);
            lector.close();
        }catch (Exception ex){
            Log.e("Archivo", "error de escritura" + ex.getMessage());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dato = (TextView) getActivity().findViewById(R.id.lblMP);
        recyclerMP = (RecyclerView) getActivity().findViewById(R.id.recyclerMP);
        botonLeer = (Button) getActivity().findViewById(R.id.btnMP);
        botonLeer.setOnClickListener(this);
        input = getResources().openRawResource(R.raw.archivo_raw);
    }

    private void cargarDialogo(View view1){
        Dialog dlgDetalles = new Dialog(getContext());
        dlgDetalles.setContentView(R.layout.dlg_artista);
        TextView nombresDetalles = dlgDetalles.findViewById(R.id.lblNombresArtistadlg);
        TextView apellidosDetalles = dlgDetalles.findViewById(R.id.lblApellidosArtistadlg);
        TextView nombreArtisticoDetalles = dlgDetalles.findViewById(R.id.lblNombreArtisticodlg);
        ImageView fotoDetalles= dlgDetalles.findViewById(R.id.fotoArtistadlg);
        nombresDetalles.setText("Nombres: "+ ListArtista.get(recyclerMP.getChildAdapterPosition(view1)).getNombres());
        apellidosDetalles.setText("Apellidos: "+ ListArtista.get(recyclerMP.getChildAdapterPosition(view1)).getApellidos());
        nombreArtisticoDetalles.setText(ListArtista.get(recyclerMP.getChildAdapterPosition(view1)).getNombreArtistico());
        fotoDetalles.setImageResource((ListArtista.get(recyclerMP.getChildAdapterPosition(view1)).getFoto() ));
        dlgDetalles.show();
    }
}
