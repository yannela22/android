package com.example.castroyannela.vistas.adapter;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Artista;

import java.util.List;

public class ArtistaAdapter extends RecyclerView.Adapter<ArtistaAdapter.ViewHolderArtista> implements View.OnClickListener {

    List<Artista> lista;        //lista tiene objetos de la clase artista
    private View.OnClickListener clickListener;

    public ArtistaAdapter(List<Artista> lista){
        this.lista = lista;
    }


    @Override
    public ViewHolderArtista onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_artista, null);
        view.setOnClickListener(this);
        return new ViewHolderArtista(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderArtista viewHolderArtista, int pos) {
        viewHolderArtista.datoNombres.setText(lista.get(pos).getNombres());
        viewHolderArtista.datoApellidos.setText(lista.get(pos).getApellidos());
        viewHolderArtista.datoNombreArtistico.setText(lista.get(pos).getNombreArtistico());
        if (lista.get(pos).getImagen() != null){
            Log.e("Error", "Entra" + lista.get(pos).getImagen());
            viewHolderArtista.fotoArtista.setImageURI(Uri.parse(lista.get(pos).getImagen()));
        } else {
            Log.e("error", "sale");
            viewHolderArtista.fotoArtista.setImageResource(lista.get(pos).getFoto());
        }

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.clickListener = listener;
    }

    @Override
    public void onClick(View view) {
        if(clickListener!= null){
            clickListener.onClick(view);
        }
    }

    public static class ViewHolderArtista extends RecyclerView.ViewHolder{
        TextView datoNombres;
        TextView datoApellidos;
        TextView datoNombreArtistico;
        ImageView fotoArtista;

        public ViewHolderArtista(View itemView) {
            super(itemView);
            datoNombres = itemView.findViewById(R.id.lblNombresArtista);
            datoApellidos = itemView.findViewById(R.id.lblApellidosArtista);
            datoNombreArtistico = itemView.findViewById(R.id.lblNombreArtistico);
            fotoArtista = itemView.findViewById(R.id.lblfotoArtistas);
        }
    }
}
