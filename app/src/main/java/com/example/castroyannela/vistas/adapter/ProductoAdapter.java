package com.example.castroyannela.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Producto;

import java.util.List;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.ViewHolderProducto> implements View.OnClickListener{

    List<Producto> listaProd;
    private View.OnClickListener clickListener;

    public ProductoAdapter(List<Producto> producto) {
        this.listaProd = producto;
    }

    @NonNull
    @Override
    public ViewHolderProducto onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto,null);
        view.setOnClickListener(this);
        return new ViewHolderProducto(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductoAdapter.ViewHolderProducto holder, int position) {
        holder.codigo.setText(""+listaProd.get(position).getCodigo());
        holder.desc.setText(listaProd.get(position).getDescripcion());
        holder.precio.setText(""+listaProd.get(position).getPrecio());
        holder.cant.setText(""+listaProd.get(position).getCantidad());
    }

    @Override
    public int getItemCount() {
        return listaProd.size();
    }

    public void setOnClickListener(View.OnClickListener listener){ this.clickListener = listener;}

    @Override
    public void onClick(View v) {
        if (clickListener != null){
            clickListener.onClick(v);
        }
    }

    public static class ViewHolderProducto extends RecyclerView.ViewHolder{
        TextView codigo, desc, precio, cant;
        public ViewHolderProducto(View itemView) {
            super(itemView);
            codigo = itemView.findViewById(R.id.lblCodigo);
            desc = itemView.findViewById(R.id.lblDescrip);
            precio = itemView.findViewById(R.id.lblPrecio);
            cant = itemView.findViewById(R.id.lblCantidad);
        }
    }
}
