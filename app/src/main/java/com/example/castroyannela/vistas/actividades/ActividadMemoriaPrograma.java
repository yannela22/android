package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Artista;
import com.example.castroyannela.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ActividadMemoriaPrograma extends AppCompatActivity implements View.OnClickListener {

    Button botonLeer;
    TextView dato;
    RecyclerView recyclerMP;
    InputStream input;
    ArtistaAdapter adapter;
    List<Artista> ListArtista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_programa);
        //cargarCadena();
        tomarControl();
    }

    public void tomarControl(){
        dato = findViewById(R.id.lblMP);
        recyclerMP = findViewById(R.id.recyclerMP);
        botonLeer = findViewById(R.id.btnMP);
        botonLeer.setOnClickListener(this);
        input = getResources().openRawResource(R.raw.archivo_raw);
    }



    @Override
    public void onClick(View view) {
        try{
            ListArtista = new ArrayList<Artista>();
            BufferedReader lector = new BufferedReader(new InputStreamReader(input));
            String listaartista = lector.readLine();
            String[] data = listaartista.split(";");
            String[] data2;
            for (int i=0; i < data.length; i++){
                data2 = data[i].split(",");
                Artista a = new Artista();
                a.setNombres(data2[0]);
                a.setApellidos(data2[1]);
                a.setNombreArtistico(data2[2]);
                a.setFoto(Integer.parseInt(data2[3]));
                //vamos añadir los datos a  la lista
                ListArtista.add(a);
            }
            adapter = new ArtistaAdapter(ListArtista);
            recyclerMP.setLayoutManager(new LinearLayoutManager(this));
            recyclerMP.setAdapter(adapter);
            lector.close();
        }catch (Exception ex){
            Log.e("ArchivoMI", "error de escritura" + ex.getMessage());
        }
    }
}
