package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Reyes;
import com.example.castroyannela.vistas.adapter.ReyesAdapter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class ActividadArchivoProgramaReyes extends AppCompatActivity {

    TextView datos;
    RecyclerView recycler;
    List<Reyes> reyesLista;
    ReyesAdapter reyesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_reyes);
        cargarComponentes();
        obtenerReyesArchivo();
    }

    public void cargarComponentes(){
        datos = findViewById(R.id.lblReyes);
        recycler = findViewById(R.id.recyclerRey);
    }

    private void obtenerReyesArchivo(){
        reyesLista = new ArrayList<Reyes>();

        try{
            InputStream inputStream = getResources().openRawResource(R.raw.rey_raw);
            DocumentBuilderFactory  factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(inputStream);
            document.getDocumentElement().normalize();
            NodeList Listgodo= document.getElementsByTagName("godo");

            Toast.makeText(this, "numero de godos es " + Listgodo.getLength(),Toast.LENGTH_SHORT).show();
            datos.setText("numero de reyes es " + Listgodo.getLength());
            for (int i =0;i<Listgodo.getLength();i++){
                Node node = Listgodo.item(i);
                if(node.getNodeType()==Node.ELEMENT_NODE){
                    Element element = (Element) node;
                    String nombres = element.getElementsByTagName("nombre").item(0).getTextContent();
                    String periodos = element.getElementsByTagName("periodo").item(0).getTextContent();
                    Reyes reyes = new Reyes();
                    reyes.setNombres(nombres);
                    reyes.setPeriodo(periodos);
                    reyesLista.add(reyes);
                }
            }
            recycler.setLayoutManager(new LinearLayoutManager(this));
            reyesAdapter = new ReyesAdapter(reyesLista);
            recycler.setAdapter(reyesAdapter);

        }catch (Exception ex){
            Log.e("Error", "Datos lista" + reyesLista);
        }
    }
}
