package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.net.Uri;
import android.os.Bundle;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Comunicador;
import com.example.castroyannela.vistas.fragmentos.Frag1;
import com.example.castroyannela.vistas.fragmentos.Frag2;

public class ActividadEscucharFragmento extends AppCompatActivity implements Comunicador, Frag2.OnFragmentInteractionListener, Frag1.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_escuchar_fragmento);
    }

    @Override
    public void responder(String datos) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Frag2 frgr = (Frag2) fragmentManager.findFragmentById(R.id.fragRecibir);
        frgr.cambiarTexto(datos);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
