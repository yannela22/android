package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.castroyannela.R;
import com.example.castroyannela.controlador.ServicioWebAlumno;
import com.example.castroyannela.modelo.Alumno;
import com.example.castroyannela.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class ActividadSWAlumnos extends AppCompatActivity implements View.OnClickListener {

    EditText cajaId, cajaNombre, cajaDireccion;
    ImageButton botonGuardar, botonModificar, botonEliminar, botonListar, botonBuscarID;
    TextView datos;
    RecyclerView recyclerSW;
    RequestQueue mQueue;
    AlumnoAdapter adapter;
    List<Alumno> lista;
    // definimos las url del servicio web
    String host = "https://reneguaman.000webhostapp.com";
    String insert = "/insertar_alumno.php";
    String get = "/obtener_alumnos.php";
    String getById = "/obtener_alumno_por_id.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";
    JSONObject json = null;

    ServicioWebAlumno sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swalumnos);
        mQueue= Volley.newRequestQueue(this);
        cargarComponentes();
    }

    private void JsonParse(String url) {
        Log.e("metodo","conectado");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("alumnos");
                            lista = new ArrayList<Alumno>();
                            for (int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject alumnos = jsonArray.getJSONObject(i);
                                Alumno alumno = new Alumno();
                                alumno.setId(alumnos.getInt("idalumno"));
                                alumno.setNombre(alumnos.getString("nombre"));
                                alumno.setDireccion(alumnos.getString("direccion"));

                                int id = alumnos.getInt("idalumno");
                                String nombre = alumnos.getString("nombre");
                                String direccion = alumnos.getString("direccion");

                                lista.add(alumno);
                                adapter = new AlumnoAdapter(lista);
                                recyclerSW.setLayoutManager(new LinearLayoutManager(ActividadSWAlumnos.this));
                                recyclerSW.setAdapter(adapter);
                                Log.e ("devuelve","id: "+String.valueOf(id)+", nombre: "+nombre+", direccion: "+direccion+"\n");
                                //datos.append("id: "+String.valueOf(id)+", nombre: "+nombre+", direccion: "+direccion+"\n");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void JsonParses(String url) {
        Log.e("metodo", "conectado");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                           lista = new ArrayList<Alumno>();
                            JSONObject jObject2 = response.getJSONObject("alumno");

                            Alumno a = new Alumno();
                            a.setId(Integer.parseInt(jObject2.getString("idAlumno")));
                            a.setNombre(jObject2.getString("nombre"));
                            a.setDireccion(jObject2.getString("direccion"));
                            lista.add(a);
                            adapter = new AlumnoAdapter(lista);
                            recyclerSW.setLayoutManager(new LinearLayoutManager(ActividadSWAlumnos.this));
                            recyclerSW.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void cargarComponentes(){
        cajaId = findViewById(R.id.txtIdAlumnoSWHilo);
        cajaNombre = findViewById(R.id.txtNombreAlumnoSWHilo);
        cajaDireccion = findViewById(R.id.txtDireccionAlumnoSWHilo);
        datos = findViewById(R.id.lblSW);
        recyclerSW = findViewById(R.id.recyclerAlumnoSW);
        botonGuardar = findViewById(R.id.btnGuardarSWHilo);
        botonModificar = findViewById(R.id.btnModificarSWHilo);
        botonEliminar = findViewById(R.id.btnElinimarSWHilo);
        botonListar = findViewById(R.id.btnListarTodoSWHilo);
        botonBuscarID = findViewById(R.id.btnBuscarPorIdSWHilo);
        botonGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonBuscarID.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        sw = new ServicioWebAlumno();
        switch (v.getId()){
            case R.id.btnGuardarSWHilo:
                sw.execute(host.concat(insert), "2", cajaNombre.getText().toString(), cajaDireccion.getText().toString());
                break;
            case R.id.btnModificarSWHilo:
                sw.execute(host.concat(update), "4", cajaId.getText().toString(), cajaNombre.getText().toString(), cajaDireccion.getText().toString());
                break;
            case R.id.btnElinimarSWHilo:
                sw.execute(host.concat(delete), "5", cajaId.getText().toString());
                break;
            case R.id.btnListarTodoSWHilo:
                sw.execute(host.concat(get),"1");   //ejecuta el hilo en el doInBackground
                String url = host.concat(get);
                JsonParse(url);
                break;
            case R.id.btnBuscarPorIdSWHilo:
                String valor = cajaId.getText().toString();
                sw.execute(host.concat(getById)+"?idalumno=" + valor, "3");
                String urls = host.concat(getById)+"?idalumno=" + valor;
                JsonParses(urls);
                break;
        }
    }
}
