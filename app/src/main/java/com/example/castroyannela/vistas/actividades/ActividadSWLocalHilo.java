package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.castroyannela.R;
import com.example.castroyannela.controlador.ServicioWebLocalHilo;
import com.example.castroyannela.modelo.Alumno;
import com.example.castroyannela.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadSWLocalHilo extends AppCompatActivity implements View.OnClickListener {

    EditText cajaId, cajaNombre, cajaDireccion;
    ImageButton botonGuardar, botonModificar, botonEliminar, botonListar, botonBuscarID;
    TextView datos;
    RecyclerView recyclerSW;
    RequestQueue mQueue;
    AlumnoAdapter adapter;
    List<Alumno> lista;
    // definimos las url del servicio web           //10.20.7.119 unl               //192.168.0.108 casa
    String host = "http://10.20.7.119/CastroYannela";   //escribir la ip del internet cuando se trabaja con servidor local
    String insert = "/wsJSONRegistroMovil.php";
    String get = "/wsJSONConsultarLista.php";
    String getById = "/wsJSONConsultarUsuario.php";
    String update = "/wsJSONUpdateMovil.php";
    String delete = "/wsJSONDeleteMovil.php";

    ServicioWebLocalHilo swLocal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swlocal_hilo);
        mQueue= Volley.newRequestQueue(this);
        cargarComponentes();
    }

    private void cargarComponentes(){
        cajaId = findViewById(R.id.txtIdLocalSWHilo);
        cajaNombre = findViewById(R.id.txtNombreLocalSWHilo);
        cajaDireccion = findViewById(R.id.txtProfesionLocalSWHilo);
        datos = findViewById(R.id.lblSWLocalHilo);
        recyclerSW = findViewById(R.id.recyclerLocalSWHilo);
        botonGuardar = findViewById(R.id.btnGuardarLocalSWHilo);
        botonModificar = findViewById(R.id.btnModificarLocalSWHilo);
        botonEliminar = findViewById(R.id.btnElinimarLocalSWHilo);
        botonListar = findViewById(R.id.btnListarTodoLocalSWHilo);
        botonBuscarID = findViewById(R.id.btnBuscarPorIdLocalSWHilo);
        botonGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonBuscarID.setOnClickListener(this);
    }

    private void JsonParse(String url) {
        Log.e("metodo", "conectado");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            lista = new ArrayList<Alumno>();
                            JSONArray array = response.getJSONArray("usuario");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject alumnos = array.optJSONObject(i);
                                Alumno estudiante = new Alumno();
                                estudiante.setId(alumnos.getInt("documento"));
                                estudiante.setNombre(alumnos.getString("nombre"));
                                estudiante.setDireccion(alumnos.getString("profesion"));
                                lista.add(estudiante);
                                cargar(lista);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }


    @Override
    public void onClick(View v) {
        swLocal = new ServicioWebLocalHilo();
        switch (v.getId()){
            case R.id.btnGuardarLocalSWHilo:
                swLocal.execute(host.concat(insert), "2", cajaNombre.getText().toString(), cajaDireccion.getText().toString());
                break;
            case R.id.btnModificarLocalSWHilo:
                swLocal.execute(host.concat(update), "4", cajaId.getText().toString(), cajaNombre.getText().toString(), cajaDireccion.getText().toString());
                break;
            case R.id.btnElinimarLocalSWHilo:
                String valor1 = cajaId.getText().toString();
                swLocal.execute(host.concat(delete)+"?documento=" + valor1, "5");
                break;
            case R.id.btnListarTodoLocalSWHilo:
                swLocal.execute(host.concat(get), "1");
                String urlss = host.concat(get);
                JsonParse(urlss);
                break;
            case R.id.btnBuscarPorIdLocalSWHilo:
                String valor = cajaId.getText().toString();
                swLocal.execute(host.concat(getById)+"?documento=" + valor, "3");
                String urls = host.concat(getById)+"?documento=" + valor;
                JsonParse(urls);
                break;
        }
    }
    private void cargar(List<Alumno>listas){
        adapter = new AlumnoAdapter(listas);
        recyclerSW.setLayoutManager(new LinearLayoutManager(this));
        recyclerSW.setAdapter(adapter);

    }
}
