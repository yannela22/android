package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.castroyannela.R;
import com.example.castroyannela.controlador.ServicioWebLocalVolley;
import com.example.castroyannela.controlador.ServicioWebVolleyAlumno;
import com.example.castroyannela.modelo.Alumno;
import com.example.castroyannela.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActividadSWLocalVolley extends AppCompatActivity implements View.OnClickListener {

    EditText cajaId, cajaNombre, cajaDireccion;
    ImageButton botonGuardar, botonModificar, botonEliminar, botonListar, botonBuscarID;
    TextView datos;
    RecyclerView recyclerSW;
    AlumnoAdapter adapter;
    List<Alumno> lista;
    ServicioWebLocalVolley swvLocal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swlocal_volley);
        cargarComponentes();
    }

    private void cargarComponentes(){
        cajaId = findViewById(R.id.txtIdLocalSWVolley);
        cajaNombre = findViewById(R.id.txtNombreLocalSWVolley);
        cajaDireccion = findViewById(R.id.txtProfesionLocalSWVolley);
        datos = findViewById(R.id.lblSWLocalVolley);
        recyclerSW = findViewById(R.id.recyclerSWLocalVolley);
        botonGuardar = findViewById(R.id.btnGuardarLocalSWVolley);
        botonModificar = findViewById(R.id.btnModificarLocalSWVolley);
        botonEliminar = findViewById(R.id.btnElinimarLocalSWVolley);
        botonListar = findViewById(R.id.btnListarTodoLocalSWVolley);
        botonBuscarID = findViewById(R.id.btnBuscarPorIdLocalSWVolley);
        botonGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonBuscarID.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        swvLocal = new ServicioWebLocalVolley(this);
        switch (v.getId()){
            case R.id.btnGuardarLocalSWVolley:
                final Map<String, String> parameters = new HashMap<String, String>();
                String docu = cajaId.getText().toString();
                String nom = cajaNombre.getText().toString();
                String pro = cajaDireccion.getText().toString();
                parameters.put("documento", docu);
                parameters.put("nombre", nom);
                parameters.put("profesion",pro);
                swvLocal.insertarStudents(parameters, new ServicioWebLocalVolley.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(ActividadSWLocalVolley.this, ""+message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                });

                break;
            case R.id.btnModificarLocalSWVolley:
                final Map<String, String> paramete = new HashMap<String, String>();
                String docum = cajaId.getText().toString();
                String nomb= cajaNombre.getText().toString();
                String prof = cajaDireccion.getText().toString();
                paramete.put("documento", docum);
                paramete.put("nombre", nomb);
                paramete.put("profesion",prof);
                swvLocal.updateUser(paramete, new ServicioWebLocalVolley.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(ActividadSWLocalVolley.this, ""+message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                });

                break;
            case R.id.btnElinimarLocalSWVolley:
                Alumno al = new Alumno();
                al.setId(Integer.parseInt(cajaId.getText().toString()));
                swvLocal.deleteUser(al, new ServicioWebLocalVolley.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(ActividadSWLocalVolley.this, ""+message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                });
                break;
            case R.id.btnListarTodoLocalSWVolley:
                swvLocal.findAllUsers(new ServicioWebLocalVolley.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray jsonArray = response.getJSONArray("usuario");
                            lista = new ArrayList<Alumno>();
                            for (int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject alumnos = jsonArray.getJSONObject(i);
                                Alumno alumno = new Alumno();
                                alumno.setId(alumnos.getInt("documento"));
                                alumno.setNombre(alumnos.getString("nombre"));
                                alumno.setDireccion(alumnos.getString("profesion"));

                                lista.add(alumno);
                                cargar(lista);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                break;
            case R.id.btnBuscarPorIdLocalSWVolley:
                Alumno alumno = new Alumno();
                alumno.setId(Integer.parseInt(cajaId.getText().toString()));
                swvLocal.getUserById(alumno, new ServicioWebLocalVolley.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("usuario");
                            lista = new ArrayList<Alumno>();
                            for (int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject alumnos = jsonArray.getJSONObject(i);
                                Alumno alumno = new Alumno();
                                alumno.setId(alumnos.getInt("documento"));
                                alumno.setNombre(alumnos.getString("nombre"));
                                alumno.setDireccion(alumnos.getString("profesion"));

                                lista.add(alumno);
                                cargar(lista);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;
        }
    }
    private void cargar(List<Alumno> alumnos){
        adapter = new AlumnoAdapter(alumnos);
        recyclerSW.setLayoutManager(new LinearLayoutManager(this));
        recyclerSW.setAdapter(adapter);

    }
}
