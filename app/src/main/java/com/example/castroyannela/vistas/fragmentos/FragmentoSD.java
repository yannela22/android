package com.example.castroyannela.vistas.fragmentos;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Artista;
import com.example.castroyannela.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentoSD.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentoSD#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoSD extends Fragment implements View.OnClickListener{
    TextView datos, imagen;
    EditText cajaNombres, cajaApellidos, cajaNombreArtistico;
    Button botonGuardar, botonListar, botonFoto;
    ImageView imagenArtista;
    RecyclerView recyclerSD;
    ArtistaAdapter adapter;
    List<Artista> artistaList;
    private String fotoArtista;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentoSD() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentoSD.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentoSD newInstance(String param1, String param2) {
        FragmentoSD fragment = new FragmentoSD();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.activity_actividad_memoria_sd, container, false);
        recyclerSD = (RecyclerView) view.findViewById(R.id.recyclerSD);
        botonListar = (Button) view.findViewById(R.id.btnListarSD);
        botonListar.setOnClickListener(this);
        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnGuardarSD:
                BufferedWriter bufferedWriter = null;
                FileWriter fileWriter = null;
                try {
                    File file = Environment.getExternalStorageDirectory();      //ruta del SD
                    File ruta = new File(file.getAbsoluteFile(),"Frgarchivo.txt");
                    fileWriter = new FileWriter(ruta.getAbsoluteFile(), true);
                    bufferedWriter = new BufferedWriter(fileWriter);
                    Artista artista = new Artista();
                    artista.setNombres(cajaNombres.getText().toString());
                    artista.setApellidos(cajaApellidos.getText().toString());
                    artista.setNombreArtistico(cajaNombreArtistico.getText().toString());
                    artista.setFoto(R.drawable.luis_miguel);
                    artista.setImagen(imagen.getText().toString());
                    bufferedWriter.write(artista.getNombres() +","+ artista.getApellidos()+","+ artista.getNombreArtistico() +","+ artista.getImagen()  + ";");
                    bufferedWriter.close();
                    Toast.makeText(getActivity().getApplicationContext(),"se guardo" ,Toast.LENGTH_SHORT ).show();
                }catch (Exception ex){
                    Log.e("Error SD", ex.getMessage());
                }
                break;
            case R.id.btnListarSD:
                try {
                    artistaList = new ArrayList<Artista>();
                    File ruta = Environment.getExternalStorageDirectory(); // ruta del SD
                    File file = new File(ruta.getAbsoluteFile(), "Frgarchivo.txt");
                    BufferedReader lector = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                    String lineas = lector.readLine();
                    String[] artis = lineas.split(";");
                    String[] arrayArchivo;
                    for (int i = 0; i < artis.length; i++) {
                        arrayArchivo = artis[i].split(",");
                        Artista artista = new Artista();
                        artista.setNombres(arrayArchivo[0]);
                        artista.setApellidos(arrayArchivo[1]);
                        artista.setNombreArtistico(arrayArchivo[2]);
                        artista.setImagen(Uri.parse(arrayArchivo[3]).toString());
                        artistaList.add(artista);
                    }
                    //datos.setText(lector.readLine());
                    adapter = new ArtistaAdapter(artistaList);
                    recyclerSD.setLayoutManager(new LinearLayoutManager(getContext()));
                    adapter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view1) {
                            Dialog dlgDetalles = new Dialog(getContext());
                            dlgDetalles.setContentView(R.layout.dlg_artista);
                            TextView nombresDetalles = dlgDetalles.findViewById(R.id.lblNombresArtistadlg);
                            TextView apellidosDetalles = dlgDetalles.findViewById(R.id.lblApellidosArtistadlg);
                            TextView nombreArtisticoDetalles = dlgDetalles.findViewById(R.id.lblNombreArtisticodlg);
                            ImageView fotoDetalles= dlgDetalles.findViewById(R.id.fotoArtistadlg);
                            nombresDetalles.setText("Nombres: "+ artistaList.get(recyclerSD.getChildAdapterPosition(view1)).getNombres());
                            apellidosDetalles.setText("Apellidos: "+ artistaList.get(recyclerSD.getChildAdapterPosition(view1)).getApellidos());
                            nombreArtisticoDetalles.setText(artistaList.get(recyclerSD.getChildAdapterPosition(view1)).getNombreArtistico());
                            fotoDetalles.setImageResource((artistaList.get(recyclerSD.getChildAdapterPosition(view1)).getFoto() ));
                            dlgDetalles.show();
                        }
                    });
                    recyclerSD.setAdapter(adapter);
                    lector.close();
                }catch (Exception ex){
                    Log.e("Error SD", ex.getMessage());
                }
                break;
            case R.id.btnSubirFotoSD:
                cargarImagen();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        datos = (TextView) getActivity().findViewById(R.id.lblSD);
        imagen = (TextView) getActivity().findViewById(R.id.imagenSD);
        imagenArtista = (ImageView) getActivity().findViewById(R.id.fotoArtistaSD);
        cajaNombres = (EditText) getActivity().findViewById(R.id.txtNombresSD);
        cajaApellidos = (EditText) getActivity().findViewById(R.id.txtApellidosSD);
        cajaNombreArtistico= (EditText) getActivity().findViewById(R.id.txtNombreArtisticoSD);
        botonGuardar = (Button) getActivity().findViewById(R.id.btnGuardarSD);
        botonFoto = (Button) getActivity().findViewById(R.id.btnSubirFotoSD);
        botonGuardar.setOnClickListener(this);
        botonFoto.setOnClickListener(this);
    }

    private void cargarImagen() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent, "cargue imagen "), 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri path = data.getData();
            imagenArtista.setImageURI(path);
            fotoArtista = path.toString();
            imagen.setText(fotoArtista);

        }
    }
}
