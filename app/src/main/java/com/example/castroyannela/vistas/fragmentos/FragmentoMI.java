package com.example.castroyannela.vistas.fragmentos;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Artista;
import com.example.castroyannela.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentoMI.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentoMI#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoMI extends Fragment implements View.OnClickListener{
    Button botonGuardar, botonListar, botonsubirFoto;
    TextView datos, label;
    EditText cajaNombres, cajaApellidos, cajaNombreArtistico;
    ImageView imagenArtista;
    RecyclerView recyclerViewArtista;
    ArtistaAdapter adapter;
    List<Artista> ListArtista;

    private String fotoArtista;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentoMI() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentoMI.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentoMI newInstance(String param1, String param2) {
        FragmentoMI fragment = new FragmentoMI();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_actividad_mi, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnGuardarMI:
                try {
                    Artista artista = new Artista();
                    artista.setNombres(cajaNombres.getText().toString());
                    artista.setApellidos(cajaApellidos.getText().toString());
                    artista.setNombreArtistico(cajaNombreArtistico.getText().toString());
                    artista.setFoto(R.drawable.luis_miguel);
                    artista.setImagen(label.getText().toString());
                    OutputStreamWriter escritor = new OutputStreamWriter(getActivity().openFileOutput("FrgArchivoArtistas22MI.txt", Context.MODE_APPEND));
                    escritor.write(artista.getNombres() +","+ artista.getApellidos()+","+ artista.getNombreArtistico() +","+ artista.getImagen()  + ";");
                    escritor.close();
                    Toast.makeText(getActivity().getApplicationContext(),"se guardo" ,Toast.LENGTH_SHORT ).show();
                }catch (Exception ex){
                    Log.e("ArchivoMI", "error de escritura" + ex.getMessage());
                }
                break;
            case R.id.btnListarMI:
                try {
                    ListArtista = new ArrayList<Artista>();
                    BufferedReader lector = new BufferedReader(new InputStreamReader(getActivity().openFileInput("FrgArchivoArtistas22MI.txt")));
                    String listaartista = lector.readLine();
                    String[] data = listaartista.split(";");
                    String[] data2;
                    for (int i=0; i < data.length; i++){
                        data2 = data[i].split(",");
                        Artista a = new Artista();
                        a.setNombres(data2[0]);
                        a.setApellidos(data2[1]);
                        a.setNombreArtistico(data2[2]);
                        a.setImagen(Uri.parse(data2[3]).toString());
                        //vamos añadir los datos a  la lista
                        ListArtista.add(a);
                    }
                    datos.setText(listaartista);
                    adapter = new ArtistaAdapter(ListArtista);
                    recyclerViewArtista.setLayoutManager(new LinearLayoutManager(getContext()));
                    adapter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cargarDialogo(v);
                        }
                    });
                    recyclerViewArtista.setAdapter(adapter);
                    lector.close();
                }catch (Exception ex){
                    Log.e("ArchivoMI", "error de lectura" + ex.getMessage());
                }
                break;
            case R.id.btnSubirFoto:
                cargarImagen();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        botonGuardar = (Button) getActivity().findViewById(R.id.btnGuardarMI);
        botonListar = (Button) getActivity().findViewById(R.id.btnListarMI);
        botonsubirFoto = (Button) getActivity().findViewById(R.id.btnSubirFoto);
        cajaNombres = (EditText) getActivity().findViewById(R.id.txtNombresMI);
        cajaApellidos = (EditText) getActivity().findViewById(R.id.txtApellidosMI);
        cajaNombreArtistico = (EditText) getActivity().findViewById(R.id.txtNombreArtisticoMI);
        imagenArtista = (ImageView) getActivity().findViewById(R.id.fotoArtistaMI);
        recyclerViewArtista = (RecyclerView) getActivity().findViewById(R.id.recyclerMI);
        datos = (TextView) getActivity().findViewById(R.id.lblDatosMI);
        label = (TextView) getActivity().findViewById(R.id.lblMI);
        botonGuardar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonsubirFoto.setOnClickListener(this);
    }

    private void cargarImagen() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent, "cargue imagen "), 10);
    }

    private void cargarDialogo(View view1){
        Dialog dlgDetalles = new Dialog(getContext());
        dlgDetalles.setContentView(R.layout.dlg_artista);
        TextView nombresDetalles = dlgDetalles.findViewById(R.id.lblNombresArtistadlg);
        TextView apellidosDetalles = dlgDetalles.findViewById(R.id.lblApellidosArtistadlg);
        TextView nombreArtisticoDetalles = dlgDetalles.findViewById(R.id.lblNombreArtisticodlg);
        ImageView fotoDetalles= dlgDetalles.findViewById(R.id.fotoArtistadlg);
        nombresDetalles.setText("Nombres: "+ ListArtista.get(recyclerViewArtista.getChildAdapterPosition(view1)).getNombres());
        apellidosDetalles.setText("Apellidos: "+ ListArtista.get(recyclerViewArtista.getChildAdapterPosition(view1)).getApellidos());
        nombreArtisticoDetalles.setText(ListArtista.get(recyclerViewArtista.getChildAdapterPosition(view1)).getNombreArtistico());
        fotoDetalles.setImageResource((ListArtista.get(recyclerViewArtista.getChildAdapterPosition(view1)).getFoto() ));
        dlgDetalles.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri path = data.getData();
            imagenArtista.setImageURI(path);
            fotoArtista = path.toString();
            label.setText(fotoArtista);

        }
    }
}
