package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.castroyannela.R;

public class ActividadEnviarParametro extends AppCompatActivity implements View.OnClickListener {

    EditText nombre,apellido;
    Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_enviar_parametro);
        cargarComponentes();
    }

    private void cargarComponentes(){
        nombre = (EditText) findViewById(R.id.txtNombreEnviarParametro);
        apellido = (EditText) findViewById(R.id.txtApellidoEnviarParametro);
        boton = (Button) findViewById(R.id.btnEnviarParametro);
        boton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(ActividadEnviarParametro.this, ActividadRecibirParametro.class);
        Bundle bundle = new Bundle();
        bundle.putString("nombres",nombre.getText().toString());
        bundle.putString("apellidos",apellido.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
