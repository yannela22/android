package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Artista;
import com.example.castroyannela.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class ActividadMI extends AppCompatActivity implements View.OnClickListener {

    Button botonGuardar, botonListar;
    TextView datos, messagge;
    EditText cajaNombres, cajaApellidos, cajaNombreArtistico;
    ImageView imagenArtista;
    RecyclerView recyclerViewArtista;
    ArtistaAdapter adapter;
    List<Artista> ListArtista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_mi);
        tomarControl();
    }

    private void tomarControl(){
        botonGuardar = findViewById(R.id.btnGuardarMI);
        botonListar = findViewById(R.id.btnListarMI);
        cajaNombres = findViewById(R.id.txtNombresMI);
        cajaApellidos = findViewById(R.id.txtApellidosMI);
        cajaNombreArtistico = findViewById(R.id.txtNombreArtisticoMI);
        imagenArtista = findViewById(R.id.fotoArtistaMI);
        datos = findViewById(R.id.lblDatosMI);
        recyclerViewArtista = findViewById(R.id.recyclerMI);
        botonGuardar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnGuardarMI:
                try {
                    Artista artista = new Artista();
                    artista.setNombres(cajaNombres.getText().toString());
                    artista.setApellidos(cajaApellidos.getText().toString());
                    artista.setNombreArtistico(cajaNombreArtistico.getText().toString());
                    artista.setFoto(R.drawable.luis_miguel);

                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivos1.txt", Context.MODE_APPEND));
                    escritor.write(artista.getNombres() +","+ artista.getApellidos()+","+ artista.getNombreArtistico() +","+ artista.getFoto()  + ";");

                    Dialog mensaje = new Dialog(ActividadMI.this);
                    mensaje.setContentView(R.layout.dlg_guardado);
                    messagge = mensaje.findViewById(R.id.dlgMensajeMI);
                    messagge.setText("Guardado Correctamente");
                    messagge.getText();
                    mensaje.show();
                    escritor.close();
                }catch (Exception ex){
                    Dialog mensaje = new Dialog(ActividadMI.this);
                    mensaje.setContentView(R.layout.dlg_guardado);
                    messagge = mensaje.findViewById(R.id.dlgMensajeMI);
                    messagge.setText("No se guardo");
                    messagge.getText();
                    mensaje.show();
                    Log.e("ArchivoMI", "error de escritura" + ex.getMessage());
                }
                break;
            case R.id.btnListarMI:
                ListArtista = new ArrayList<Artista>();
                try {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivos1.txt")));
                    String listaartista = lector.readLine();
                    String[] data = listaartista.split(";");
                    String[] data2;
                    for (int i=0; i < data.length; i++){
                        data2 = data[i].split(",");
                        Artista a = new Artista();
                        a.setNombres(data2[0]);
                        a.setApellidos(data2[1]);
                        a.setNombreArtistico(data2[2]);
                        a.setFoto(Integer.parseInt(data2[3]));
                        //vamos añadir los datos a  la lista
                        ListArtista.add(a);
                    }
                    datos.setText(listaartista);
                    adapter = new ArtistaAdapter(ListArtista);
                    recyclerViewArtista.setLayoutManager(new LinearLayoutManager(this));
                    recyclerViewArtista.setAdapter(adapter);
                    lector.close();
                }catch (Exception ex){
                    Log.e("ArchivoMI", "error de escritura" + ex.getMessage());
                }
                break;
        }
    }
}
