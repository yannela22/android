package com.example.castroyannela.vistas.actividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.castroyannela.R;
import com.example.castroyannela.vistas.fragmentos.FragmentoMI;
import com.example.castroyannela.vistas.fragmentos.FragmentoPrograma;
import com.example.castroyannela.vistas.fragmentos.FragmentoSD;

public class ActividadArchivo extends AppCompatActivity implements FragmentoMI.OnFragmentInteractionListener, FragmentoSD.OnFragmentInteractionListener, FragmentoPrograma.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_archivo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_archivo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemMI:
                FragmentoMI fragmentoMI = new FragmentoMI();
                FragmentTransaction transaccion1 = getSupportFragmentManager().beginTransaction();
                transaccion1.replace(R.id.contenedorArtista, fragmentoMI);
                transaccion1.commit();
                break;
            case R.id.itemSD:
                FragmentoSD fragmentoSD = new FragmentoSD();
                FragmentTransaction transaccion2 = getSupportFragmentManager().beginTransaction();
                transaccion2.replace(R.id.contenedorArtista, fragmentoSD);
                transaccion2.commit();
                break;
            case R.id.itemPrograma:
                FragmentoPrograma fragmentoP = new FragmentoPrograma();
                FragmentTransaction transaccion3 = getSupportFragmentManager().beginTransaction();
                transaccion3.replace(R.id.contenedorArtista, fragmentoP);
                transaccion3.commit();
                break;
        }
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
