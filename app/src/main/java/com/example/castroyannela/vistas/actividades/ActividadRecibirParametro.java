package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.castroyannela.R;

public class ActividadRecibirParametro extends AppCompatActivity {

    TextView recibirNombre, recibirApellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recibir_parametro);
        cargarComponentes();
    }

    private void cargarComponentes(){
        recibirNombre = (TextView) findViewById(R.id.lblNombreRecibirParametro);
        recibirApellido = (TextView) findViewById(R.id.lblApellidoRecibirParametro);
        Bundle bundle = this.getIntent().getExtras();
        recibirNombre.setText(bundle.getString("nombres"));
        recibirApellido.setText(bundle.getString("apellidos"));
    }
}
