package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.example.castroyannela.R;

public class ActividadSensorLuz extends AppCompatActivity implements SensorEventListener {

    SensorManager manager;
    Sensor sensor;
    TextView x;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_luz);
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = manager.getDefaultSensor(Sensor.TYPE_LIGHT);
        cargarComponentes();
    }

    private void cargarComponentes(){
        x = findViewById(R.id.labelSensorLuz);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float luz = event.values[0];
        x.setText(luz+"");
        if (luz >= 20.0){
            getWindow().getDecorView().setBackgroundColor(Color.parseColor("#FC3980"));
        }
        if (luz <= 19.0){
            getWindow().getDecorView().setBackgroundColor(Color.parseColor("#F87CA9"));
        }
        if (luz <= 8.0){
            getWindow().getDecorView().setBackgroundColor(Color.parseColor("#F9B5CD"));
        }
        if (luz <= 4.0){
            getWindow().getDecorView().setBackgroundColor(Color.parseColor("#FCDEE8"));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this, sensor, manager.SENSOR_DELAY_NORMAL);
    }
}
