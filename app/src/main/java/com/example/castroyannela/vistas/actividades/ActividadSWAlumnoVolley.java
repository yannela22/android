package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.castroyannela.R;
import com.example.castroyannela.controlador.ServicioWebVolleyAlumno;
import com.example.castroyannela.modelo.Alumno;
import com.example.castroyannela.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadSWAlumnoVolley extends AppCompatActivity implements View.OnClickListener {

    EditText cajaId, cajaNombre, cajaDireccion;
    ImageButton botonGuardar, botonModificar, botonEliminar, botonListar, botonBuscarID;
    TextView datos;
    RecyclerView recyclerSW;
    AlumnoAdapter adapter;
    List<Alumno> lista;
    ServicioWebVolleyAlumno swv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swalumno_volley);
        cargarComponentes();
    }

    private void cargarComponentes(){
        cajaId = findViewById(R.id.txtIdAlumnoSWVolley);
        cajaNombre = findViewById(R.id.txtNombreAlumnoSWVolley);
        cajaDireccion = findViewById(R.id.txtDireccionAlumnoSWVolley);
        datos = findViewById(R.id.lblSWVolley);
        recyclerSW = findViewById(R.id.recyclerAlumnoSWVolley);
        botonGuardar = findViewById(R.id.btnGuardarSWVolley);
        botonModificar = findViewById(R.id.btnModificarSWVolley);
        botonEliminar = findViewById(R.id.btnElinimarSWVolley);
        botonListar = findViewById(R.id.btnListarTodoSWVolley);
        botonBuscarID = findViewById(R.id.btnBuscarPorIdSWVolley);
        botonGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonBuscarID.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        swv = new ServicioWebVolleyAlumno(this);
        switch (v.getId()){
            case R.id.btnGuardarSWVolley:
                Alumno a = new Alumno();
                a.setNombre(cajaNombre.getText().toString());
                a.setDireccion(cajaDireccion.getText().toString());
                swv.insertStudents(a);
                break;
            case R.id.btnModificarSWVolley:
                Alumno al = new Alumno();
                al.setId(Integer.parseInt(cajaId.getText().toString()));
                al.setNombre(cajaNombre.getText().toString());
                al.setDireccion(cajaDireccion.getText().toString());
                swv.updateStudent(al);
                break;
            case R.id.btnElinimarSWVolley:
                Alumno alumno = new Alumno();
                alumno.setId(Integer.parseInt(cajaId.getText().toString()));
                swv.deleteStudent(alumno);
                break;
            case R.id.btnListarTodoSWVolley:
                swv.findAllStudents(new ServicioWebVolleyAlumno.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("alumnos");
                            lista = new ArrayList<Alumno>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject alumnos = jsonArray.getJSONObject(i);
                                Alumno alumno = new Alumno();
                                alumno.setId(alumnos.getInt("idalumno"));
                                alumno.setNombre(alumnos.getString("nombre"));
                                alumno.setDireccion(alumnos.getString("direccion"));
                                lista.add(alumno);
                                cargarRecycler(lista);

                            }
                        }catch (Exception e){

                        }
                    }
                });

                break;
            case R.id.btnBuscarPorIdSWVolley:
                lista = new ArrayList<Alumno>();
                Alumno alum = new Alumno();
                int i = Integer.parseInt(cajaId.getText().toString());
                alum.setId(i);
                swv.getStudentById(alum, new ServicioWebVolleyAlumno.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            lista = new ArrayList<Alumno>();
                            JSONObject object = response.getJSONObject("alumno");
                            Alumno estudiante = new Alumno();
                            estudiante.setId(Integer.parseInt(object.getString("idAlumno")));
                            estudiante.setNombre(object.getString("nombre"));
                            estudiante.setDireccion(object.getString("direccion"));
                            lista.add(estudiante);
                            datos.setText(Integer.parseInt(object.getString("idAlumno")) +object.getString("nombre")
                                    + object.getString("direccion"));
                            cargarRecycler(lista);
                        } catch (Exception ex){

                        }
                    }
                });
                Log.e("data" , "" + lista);
                break;
        }
    }

    private void cargarRecycler(List<Alumno> alumnos){
        adapter = new AlumnoAdapter(alumnos);
        recyclerSW.setLayoutManager(new LinearLayoutManager(this));
        recyclerSW.setAdapter(adapter);
    }
}
