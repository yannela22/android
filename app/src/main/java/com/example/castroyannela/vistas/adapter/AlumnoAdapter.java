package com.example.castroyannela.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Alumno;

import java.util.List;

public class AlumnoAdapter extends RecyclerView.Adapter<AlumnoAdapter.ViewHolderAlumno> implements View.OnClickListener{

    List<Alumno> listaAlumno;
    private View.OnClickListener clickListener;

    public AlumnoAdapter(List<Alumno> alumno){this.listaAlumno = alumno;}

    @NonNull
    @Override
    public ViewHolderAlumno onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alumno,null);
        view.setOnClickListener(this);
        return new ViewHolderAlumno(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderAlumno holder, int pos) {
        holder.id.setText(""+listaAlumno.get(pos).getId());
        holder.nombre.setText(listaAlumno.get(pos).getNombre());
        holder.direccion.setText(listaAlumno.get(pos).getDireccion());
    }

    @Override
    public int getItemCount() {
        return listaAlumno.size();
    }

    public void setOnClickListener(View.OnClickListener listener){ this.clickListener = listener;}

    @Override
    public void onClick(View v) {
        if (clickListener != null){
            clickListener.onClick(v);
        }
    }

    public static class ViewHolderAlumno extends RecyclerView.ViewHolder{
        TextView id, nombre, direccion;
        public ViewHolderAlumno(View itemView){
            super(itemView);
            id = itemView.findViewById(R.id.lblIdSW);
            nombre = itemView.findViewById(R.id.lblNombreSW);
            direccion = itemView.findViewById(R.id.lblDireccionSW);
        }
    }
}
