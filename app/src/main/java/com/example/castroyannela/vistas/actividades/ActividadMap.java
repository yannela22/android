package com.example.castroyannela.vistas.actividades;

import androidx.fragment.app.FragmentActivity;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.castroyannela.R;
import com.example.castroyannela.controlador.Rutas;
import com.example.castroyannela.modelo.Ruta;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

public class ActividadMap extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener{

    private GoogleMap mMap;
    private Button botonSatelite, botonTerreno, botonHibrido, botonpolilineas;
    private LatLngBounds CASA = new LatLngBounds(new LatLng(-4.016391,-79.211305), new LatLng(-4.016391,-79.211305));
    List<Ruta> listaRuta;
    Rutas ruta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_map);
        cargarComponentes();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ruta = new Rutas(this);
    }

    private void cargarComponentes(){
        botonpolilineas = findViewById(R.id.btnpolilineasmapas);
        botonSatelite = findViewById(R.id.btnSatelite);
        botonTerreno = findViewById(R.id.btnTerreno);
        botonHibrido = findViewById(R.id.btnHibrido);
        botonSatelite.setOnClickListener(this);
        botonTerreno.setOnClickListener(this);
        botonHibrido.setOnClickListener(this);
        botonpolilineas.setOnClickListener(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng home = new LatLng(-4.016391,-79.211305);
        mMap.addMarker(new MarkerOptions().position(home).title("Casa"));
        circulo();
        float zoom = 15.0f;
        mMap.setMinZoomPreference(10);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(home, zoom));
        PolylineOptions lineas = new PolylineOptions();
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Dialog dlgMapa = new Dialog(ActividadMap.this);
                dlgMapa.setContentView(R.layout.dlg_mapa);
                TextView titulo = dlgMapa.findViewById(R.id.lblMapasTitulo);
                TextView descripcion = dlgMapa.findViewById(R.id.lblMapasDesc);
                ImageView imagen= dlgMapa.findViewById(R.id.imgMapasDesc);
                for (Ruta rut:listaRuta) {
                    if(marker.getTitle().equals(rut.getTitulo())){
                        titulo.setText(rut.getTitulo());
                        descripcion.setText(rut.getDesc());
                        int id = getResources().getIdentifier(rut.getFoto(), "drawable", getPackageName());
                        imagen.setImageResource(id);
                    }
                }
                dlgMapa.show();

            }
        });
        listaRuta = ruta.findResource();
        for (int i = 0; i <ruta.findResource().size(); i++) {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(listaRuta.get(i).getLatitud(),listaRuta.get(i).getLongitud()))
                    .title(listaRuta.get(i).getTitulo())
                    .snippet(listaRuta.get(i).getDescripcion())
                    .icon(BitmapDescriptorFactory.fromBitmap(obetnerI(listaRuta.get(i).getIcono())))
            );
            lineas.add(new LatLng(listaRuta.get(i).getLatitud(),listaRuta.get(i).getLongitud()))
                    .width(9).color(Color.parseColor("#FB09D3"));

        }
        mMap.addPolyline(lineas);

    }

    private void poligono(){
        PolylineOptions lineas = new PolylineOptions();
        lineas.add(new LatLng(-4.032583, -79.202496)).width(9).color(Color.BLACK);
        mMap.addPolyline(lineas);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSatelite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mostrar();
                break;
            case R.id.btnTerreno:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                obtenerPosicion();
                break;
            case R.id.btnHibrido:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CASA.getCenter(), 15));
                mMap.setMinZoomPreference(10);
                break;
            case R.id.btnpolilineasmapas:
                mostrarRuta();
                break;
        }
    }

    public Bitmap obetnerI(String icon){
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(icon, "drawable", getPackageName()));
        return bitmap;
    }
    private void mostrarRuta(){
        PolylineOptions rectangulo = new PolylineOptions().add(new LatLng(-4.016391,-79.211305),
                new LatLng(-4.018393, -79.210960), new LatLng(-4.012361, -79.204556));
        rectangulo.color(Color.parseColor("#ff0000"));
        mMap.addPolyline(rectangulo);
    }
    private void mostrar(){
        LatLng casa = new LatLng(-4.016391,-79.211305);
        CameraPosition camPos = new CameraPosition.Builder().target(casa).zoom(20).bearing(90).tilt(70).build();
        CameraUpdate cam = CameraUpdateFactory.newCameraPosition(camPos);
        mMap.animateCamera(cam);
    }
    private void obtenerPosicion(){
        CameraPosition camPos = mMap.getCameraPosition();
        LatLng coordenadas = camPos.target;
        double latitud = coordenadas.latitude;
        double longitud = coordenadas.longitude;
        Toast.makeText(this, "Lat: " + latitud + " | Long: " + longitud, Toast.LENGTH_SHORT).show();
    }
    private void circulo(){
        CircleOptions c = new CircleOptions().center(new LatLng(-4.016391,-79.211305)).radius(10)
                .strokeColor(Color.parseColor("#ff0000")).strokeWidth(4)
                .fillColor(Color.argb(62,100,50,120));
        mMap.addCircle(c);
    }
}
