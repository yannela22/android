package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Artista;
import com.example.castroyannela.vistas.adapter.ArtistaAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActividadRecyclerArtistas extends AppCompatActivity {

    RecyclerView recyclerViewArtistas;
    ArtistaAdapter adapter;
    List<Artista> listaArtistas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recycler_artistas);
        tomarControl();
        cargarRecycler();
    }

    private void tomarControl(){
        recyclerViewArtistas = findViewById(R.id.recyclerArtistas);
    }

    private void cargarRecycler(){
        Artista artista1 = new Artista();
        artista1.setNombres("Luis");
        artista1.setApellidos("Miguel");
        artista1.setNombreArtistico("Luis Miguel");
        artista1.setFoto(R.drawable.luis_miguel);

        Artista artista2 = new Artista();
        artista2.setNombres("Don");
        artista2.setApellidos("Medardo");
        artista2.setNombreArtistico("Don Medardo y sus Players");
        artista2.setFoto(R.drawable.donmedardo);

        Artista artista3 = new Artista();
        artista3.setNombres("Bruno");
        artista3.setApellidos("Mars");
        artista3.setNombreArtistico("Bruno Mars");
        artista3.setFoto(R.drawable.brunomars);

        listaArtistas = new ArrayList<Artista>();
        listaArtistas.add(artista1);
        listaArtistas.add(artista2);
        listaArtistas.add(artista3);

        adapter = new ArtistaAdapter(listaArtistas);
        recyclerViewArtistas.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewArtistas.setAdapter(adapter);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarDialogo(v);
            }
        });
    }

    public void cargarDialogo(View view){
        Dialog dialog = new Dialog(ActividadRecyclerArtistas.this);
        dialog.setContentView(R.layout.dlg_artista);
        TextView nombres = dialog.findViewById(R.id.lblNombresArtistadlg);
        TextView apellidos = dialog.findViewById(R.id.lblApellidosArtistadlg);
        TextView nombreArtistico = dialog.findViewById(R.id.lblNombreArtisticodlg);
        ImageView imageView = dialog.findViewById(R.id.fotoArtistadlg);
        nombres.setText(listaArtistas.get(recyclerViewArtistas.getChildAdapterPosition(view)).getNombres());
        apellidos.setText(listaArtistas.get(recyclerViewArtistas.getChildAdapterPosition(view)).getApellidos());
        nombreArtistico.setText(listaArtistas.get(recyclerViewArtistas.getChildAdapterPosition(view)).getNombreArtistico());
        imageView.setImageResource( listaArtistas.get(recyclerViewArtistas.getChildAdapterPosition(view)).getFoto());
        dialog.show();
    }
}
