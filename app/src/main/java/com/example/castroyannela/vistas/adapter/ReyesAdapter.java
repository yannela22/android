package com.example.castroyannela.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Reyes;

import java.util.List;

public class ReyesAdapter extends RecyclerView.Adapter<ReyesAdapter.ViewHolderReyes> {
    List<Reyes>reyesList;
    public ReyesAdapter(List<Reyes> reyes) {
        this.reyesList = reyes;
    }

    @NonNull
    @Override
    public ReyesAdapter.ViewHolderReyes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reyes,null);

        return new ViewHolderReyes(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ReyesAdapter.ViewHolderReyes holder, int position) {
        holder.datoNombre.setText(reyesList.get(position).getNombres());
        holder.datoPeriodo.setText(reyesList.get(position).getPeriodo());
    }

    @Override
    public int getItemCount() {
        return reyesList.size();
    }
    public static class ViewHolderReyes extends RecyclerView.ViewHolder{
        TextView datoNombre, datoPeriodo;
        public ViewHolderReyes(View itemView) {
            super(itemView);
            datoNombre = itemView.findViewById(R.id.lblNombreReyes);
            datoPeriodo = itemView.findViewById(R.id.lblPeriodos);
        }
    }
}
