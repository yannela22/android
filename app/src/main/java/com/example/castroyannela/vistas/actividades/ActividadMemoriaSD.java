package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.castroyannela.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ActividadMemoriaSD extends AppCompatActivity implements View.OnClickListener {

    TextView datos;
    EditText cajaNombres, cajaApellidos;
    Button botonEscribir, botonLeer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_sd);
        cargarComponentes();
    }



    private void cargarComponentes(){
        datos = findViewById(R.id.lblSD);
        cajaNombres = findViewById(R.id.txtNombresSD);
        cajaApellidos = findViewById(R.id.txtApellidosSD);
        botonEscribir = findViewById(R.id.btnGuardarSD);
        botonLeer = findViewById(R.id.btnListarSD);
        botonEscribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnGuardarSD:
                BufferedWriter bufferedWriter = null;
                FileWriter fileWriter = null;
                try {
                    File file = Environment.getExternalStorageDirectory();      //ruta del SD
                    File ruta = new File(file.getAbsoluteFile(),"archivo.txt");
                    fileWriter = new FileWriter(ruta.getAbsoluteFile(), true);
                    bufferedWriter = new BufferedWriter(fileWriter);
                    //OutputStreamWriter escritor = new OutputStreamWriter(new FileOutputStream(file));
                    bufferedWriter.write(cajaNombres.getText().toString()+","+cajaApellidos.getText().toString()+";");
                    bufferedWriter.close();
                }catch (Exception ex){
                    Log.e("Error SD", ex.getMessage());
                }
                //OutputStreamWriter escritor = new OutputStreamWriter(file);
                break;
            case R.id.btnListarSD:
                try {
                    File ruta = Environment.getExternalStorageDirectory();      //ruta del SD
                    File file = new File(ruta.getAbsoluteFile(),"archivo.txt");
                    BufferedReader lector = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                    datos.setText(lector.readLine());
                    lector.close();
                }catch (Exception ex){
                    Log.e("Error SD", ex.getMessage());
                }
                break;
        }
    }


}
