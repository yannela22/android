package com.example.castroyannela.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.castroyannela.R;
import com.example.castroyannela.controlador.HelperProducto;
import com.example.castroyannela.modelo.Producto;
import com.example.castroyannela.vistas.adapter.ProductoAdapter;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;

public class ActividadProductoHelper extends AppCompatActivity implements View.OnClickListener {

    EditText cajaCodigo, cajaDescripcion, cajaPrecio, cajaCantidad;
    ImageButton botonAgregar, botonListar, botonModificar, botonEliminar, botonEliminarTodo, botonBuscarxCodigo;
    RecyclerView recycler;
    HelperProducto helper;
    List<Producto> lista;
    ProductoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_producto_helper);
        cargarComponentes();
    }

    private void cargarComponentes(){
        cajaCodigo = findViewById(R.id.txtCodigoProducto);
        cajaDescripcion = findViewById(R.id.txtDescripcionProducto);
        cajaPrecio = findViewById(R.id.txtPrecioProducto);
        cajaCantidad = findViewById(R.id.txtCantidadProducto);
        recycler = findViewById(R.id.recyclerProductoHelper);
        botonAgregar = findViewById(R.id.btnAgregrarProducto);
        botonListar = findViewById(R.id.btnListarProductos);
        botonEliminar = findViewById(R.id.btnElinimarProducto);
        botonModificar = findViewById(R.id.btnModificarProducto);
        botonEliminarTodo = findViewById(R.id.btnEliminarTodo);
        botonBuscarxCodigo = findViewById(R.id.btnBuscarbyCode);
        botonAgregar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminarTodo.setOnClickListener(this);
        botonBuscarxCodigo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAgregrarProducto:
                helper = new HelperProducto(this,"name", null, 1);
                SQLiteDatabase sql = helper.getWritableDatabase();
                Producto prod = new Producto();
                int codigo = Integer.parseInt(cajaCodigo.getText().toString());
                prod.setDescripcion(cajaDescripcion.getText().toString());
                double precio = Double.parseDouble(cajaPrecio.getText().toString());
                int cantidad = Integer.parseInt(cajaCantidad.getText().toString());
                prod.setCodigo(codigo);
                prod.setCantidad(cantidad);
                prod.setPrecio(precio);
                helper.insertar(prod);
                sql.close();
                Toast.makeText(ActividadProductoHelper.this, "Guardado Correctamente", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnListarProductos:
                helper = new HelperProducto(this,"name", null, 1);
                SQLiteDatabase sqlD = helper.getReadableDatabase();
                lista = helper.getAll();
                adapter = new ProductoAdapter(lista);
                recycler.setLayoutManager(new LinearLayoutManager(this));
                adapter.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        cajaCodigo.setText(""+lista.get(recycler.getChildAdapterPosition(view)).getCodigo());
                        cajaDescripcion.setText(""+lista.get(recycler.getChildAdapterPosition(view)).getDescripcion());
                        cajaPrecio.setText(""+lista.get(recycler.getChildAdapterPosition(view)).getPrecio());
                        cajaCantidad.setText(""+lista.get(recycler.getChildAdapterPosition(view)).getCantidad());
                    }
                });
                recycler.setAdapter(adapter);
                sqlD.close();
                break;
            case R.id.btnElinimarProducto:
                helper = new HelperProducto(this,"name", null, 1);
                SQLiteDatabase dat = helper.getWritableDatabase();
                Producto produ = new Producto();
                int co = Integer.parseInt(cajaCodigo.getText().toString());
                produ.setCodigo(co);
                helper.eliminar(produ);
                dat.close();
                Toast.makeText(ActividadProductoHelper.this, "Eliminado Correctamente", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnModificarProducto:
                helper = new HelperProducto(this,"name", null, 1);
                SQLiteDatabase dato = helper.getWritableDatabase();
                Producto produc = new Producto();
                int cod = Integer.parseInt(cajaCodigo.getText().toString());
                produc.setDescripcion(cajaDescripcion.getText().toString());
                double prec = Double.parseDouble(cajaPrecio.getText().toString());
                int cant = Integer.parseInt(cajaCantidad.getText().toString());
                produc.setCodigo(cod);
                produc.setCantidad(cant);
                produc.setPrecio(prec);
                helper.modificar(produc);
                dato.close();
                Toast.makeText(ActividadProductoHelper.this, "Modificado Correctamente", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnEliminarTodo:
                helper = new HelperProducto(this,"name", null, 1);
                SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
                helper.eliminarTodo();
                sqLiteDatabase.close();
                Toast.makeText(ActividadProductoHelper.this, "Se eliminaron todos los productos", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnBuscarbyCode:
                helper = new HelperProducto(this,"name", null, 1);
                SQLiteDatabase sqlite = helper.getReadableDatabase();
                Producto producto = new Producto();
                producto.setCodigo(Integer.parseInt(cajaCodigo.getText().toString()));
                String code = (cajaCodigo.getText().toString());
                lista = helper.getProductByCode(code);
                adapter = new ProductoAdapter(lista);
                recycler.setLayoutManager(new LinearLayoutManager(this));
                recycler.setAdapter(adapter);
                sqlite.close();
                break;
        }
    }
}
