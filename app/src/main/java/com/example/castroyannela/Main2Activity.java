package com.example.castroyannela;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import com.example.castroyannela.vistas.actividades.ActividadArchivo;
import com.example.castroyannela.vistas.actividades.ActividadArchivoProgramaReyes;
import com.example.castroyannela.vistas.actividades.ActividadCarroORM;
import com.example.castroyannela.vistas.actividades.ActividadEnviarParametro;
import com.example.castroyannela.vistas.actividades.ActividadEscucharFragmento;
import com.example.castroyannela.vistas.actividades.ActividadFragmento;
import com.example.castroyannela.vistas.actividades.ActividadMap;
import com.example.castroyannela.vistas.actividades.ActividadProductoHelper;
import com.example.castroyannela.vistas.actividades.ActividadRecyclerArtistas;
import com.example.castroyannela.vistas.actividades.ActividadSWAlumnoVolley;
import com.example.castroyannela.vistas.actividades.ActividadSWAlumnos;
import com.example.castroyannela.vistas.actividades.ActividadSWClima;
import com.example.castroyannela.vistas.actividades.ActividadSWLocalHilo;
import com.example.castroyannela.vistas.actividades.ActividadSWLocalVolley;
import com.example.castroyannela.vistas.actividades.ActividadSensorLuz;
import com.example.castroyannela.vistas.actividades.ActividadSensorProximidad;
import com.example.castroyannela.vistas.actividades.ActividadSensores;
import com.example.castroyannela.vistas.actividades.ActivityLogin;
import com.example.castroyannela.vistas.actividades.ActivitySuma;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new Fragment()).commit();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Replace", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //metodo para cargar menus
        //menuInflater permite crear un objeto para manejar el archivo xml(main.xml)
        //metodo inflate permite agregar un menu implementado de un archivo xml a la actividad
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //Este metodo permite realizar eventos en cada item hijo de los menus
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(Main2Activity.this, ActivityLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionSumar:
                intent = new Intent(Main2Activity.this, ActivitySuma.class);
                startActivity(intent);
                break;
            case R.id.opcionParametros:
                intent = new Intent(Main2Activity.this, ActividadEnviarParametro.class);
                startActivity(intent);
                break;
            case R.id.opcionFrgColores:
                intent = new Intent(Main2Activity.this, ActividadFragmento.class);
                startActivity(intent);
                break;
            case R.id.opcionFrgEscuchar:
                intent = new Intent(Main2Activity.this, ActividadEscucharFragmento.class);
                startActivity(intent);
                break;
            case R.id.opcionDlgSumar:
                final Dialog dlgSumar = new Dialog(Main2Activity.this);
                dlgSumar.setContentView(R.layout.dlg_sumar);
                final EditText cajaN1 = dlgSumar.findViewById(R.id.txtN1Dlg);
                final EditText cajaN2 = dlgSumar.findViewById(R.id.txtN2Dlg);
                Button botonSumarDlg = dlgSumar.findViewById(R.id.btnSumarDlg);
                botonSumarDlg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resultado = Integer.parseInt(cajaN1.getText().toString()) + Integer.parseInt(cajaN2.getText().toString());
                        Toast.makeText(Main2Activity.this, "Suma = " + resultado, Toast.LENGTH_SHORT).show();
                        dlgSumar.hide();
                    }
                });
                dlgSumar.show();
                break;
            case R.id.opcionArtistas:
                intent = new Intent(Main2Activity.this, ActividadRecyclerArtistas.class);
                startActivity(intent);
                break;
            case R.id.opcionArtista:
                intent = new Intent(Main2Activity.this, ActividadArchivo.class);
                startActivity(intent);
                break;
            case R.id.opcionReyes:
                intent = new Intent(Main2Activity.this, ActividadArchivoProgramaReyes.class);
                startActivity(intent);
                break;
            case R.id.opcionHelper:
                intent = new Intent(Main2Activity.this, ActividadProductoHelper.class);
                startActivity(intent);
                break;
            case R.id.opcionORM:
                intent = new Intent(Main2Activity.this, ActividadCarroORM.class);
                startActivity(intent);
                break;
            case R.id.opcionSWHilo:
                intent = new Intent(Main2Activity.this, ActividadSWAlumnos.class);
                startActivity(intent);
                break;
            case R.id.opcionSWHiloClima:
                intent = new Intent(Main2Activity.this, ActividadSWClima.class);
                startActivity(intent);
                break;
            case R.id.opcionSWVolly:
                intent = new Intent(Main2Activity.this, ActividadSWAlumnoVolley.class);
                startActivity(intent);
                break;
            case R.id.opcionSWLocalVolly:
                intent = new Intent(Main2Activity.this, ActividadSWLocalVolley.class);
                startActivity(intent);
                break;
            case R.id.opcionSWLocalHilo:
                intent = new Intent(Main2Activity.this, ActividadSWLocalHilo.class);
                startActivity(intent);
                break;
            case R.id.opcionMapas:
                intent = new Intent(Main2Activity.this, ActividadMap.class);
                startActivity(intent);
                break;
            case R.id.opcionAcelerometro:
                intent = new Intent(Main2Activity.this, ActividadSensores.class);
                startActivity(intent);
                break;
            case R.id.opcionProximidad:
                intent = new Intent(Main2Activity.this, ActividadSensorProximidad.class);
                startActivity(intent);
                break;
            case R.id.opcionLuz:
                intent = new Intent(Main2Activity.this, ActividadSensorLuz.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        Intent intent;
        if (id == R.id.nav_artista) {

            intent = new Intent(Main2Activity.this, ActividadArchivo.class);
            startActivity(intent);

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        }else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
