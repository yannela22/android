package com.example.castroyannela.modelo;

public interface Comunicador {
    public void responder(String datos);
}
