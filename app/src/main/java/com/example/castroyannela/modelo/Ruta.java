package com.example.castroyannela.modelo;

public class Ruta {
    private double Latitud;
    private double Longitud;
    private String Titulo;
    private String Descripcion;
    private String Icono;
    String desc;
    String foto;

    public Ruta(){

    }

    public Ruta(double latitud, double longitud, String titulo, String descripcion, String icono, String desc, String foto) {
        Latitud = latitud;
        Longitud = longitud;
        Titulo = titulo;
        Descripcion = descripcion;
        Icono = icono;
        this.desc = desc;
        this.foto = foto;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getIcono() {
        return Icono;
    }

    public void setIcono(String icono) {
        Icono = icono;
    }

    public double getLongitud() {
        return Longitud;
    }

    public void setLongitud(double longitud) {
        Longitud = longitud;
    }

    public double getLatitud() {
        return Latitud;
    }

    public void setLatitud(double latitud) {
        Latitud = latitud;
    }
}
