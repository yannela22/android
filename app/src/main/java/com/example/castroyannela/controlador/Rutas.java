package com.example.castroyannela.controlador;

import android.content.Context;

import com.example.castroyannela.R;
import com.example.castroyannela.modelo.Ruta;
import com.google.android.gms.maps.GoogleMap;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Rutas {
    Context context;
    GoogleMap mMap;

    public Rutas(Context context) {
        this.context = context;
    }

    public List<Ruta> findResource(){
        List<Ruta> rutaList = new ArrayList<Ruta>();
        try {
            InputStream inputStream = context.getResources().openRawResource(R.raw.rutas_raw);
            BufferedReader lector = new BufferedReader(new InputStreamReader(inputStream));
            String listaartista = lector.readLine();
            while (listaartista != null) {
                String[] data = listaartista.split(";");
                for (int i = 0; i < data.length; i++) {
                    String[] array;
                    array = data[i].split(",");
                    Ruta ruta = new Ruta();
                    ruta.setLatitud(Double.parseDouble(array[0]));
                    ruta.setLongitud(Double.parseDouble(array[1]));
                    ruta.setTitulo(array[2]);
                    ruta.setDescripcion(array[3]);
                    ruta.setIcono(array[4]);
                    ruta.setDesc(array[5]);
                    ruta.setFoto(array[6]);
                    rutaList.add(ruta);
                }
                listaartista=lector.readLine();
            }

        }catch (Exception es){

        }

        return rutaList;
    }
}
