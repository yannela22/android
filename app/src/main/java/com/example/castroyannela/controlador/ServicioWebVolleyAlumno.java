package com.example.castroyannela.controlador;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.castroyannela.modelo.Alumno;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ServicioWebVolleyAlumno {

    Context context;

    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(JSONObject response);
    }

    String host = "https://reneguaman.000webhostapp.com";
    String insert = "/insertar_alumno.php";
    String get = "/obtener_alumnos.php";
    String getById = "/obtener_alumno_por_id.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";

    String consulta;
    boolean estado;
    List<Alumno> lista;
    JSONObject json;

    public ServicioWebVolleyAlumno(Context context) {
        this.context = context;
    }

    public String findAllStudents(final VolleyResponseListener volleyResponseListener){
        String path = host.concat(get);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                volleyResponseListener.onResponse(response);
                Toast.makeText(context, "" + response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                estado = false;
            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(request);
        return consulta;
    }

    public boolean insertStudents(Alumno alumno){
        String path = host.concat(insert);
        JSONObject json = new JSONObject();
        try {
            json.put("nombre", alumno.getNombre());
            json.put("direccion", alumno.getDireccion());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                estado = true;
            } }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                estado = false;
            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(request);
        return estado;
    }

    public boolean updateStudent(Alumno alumno){
        String path = host.concat(update);
        final JSONObject json = new JSONObject();
        try {
            json.put("idalumno", alumno.getId());
            json.put("nombre", alumno.getNombre());
            json.put("direccion", alumno.getDireccion());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                Toast.makeText(context,response.toString(),Toast.LENGTH_SHORT);
                estado = true;
            } }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error.Response", error.toString());
                estado = false;
            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(request);
        return estado;
    }

    public String getStudentById(Alumno alumno, final VolleyResponseListener volleyResponseListener) {
        String path = host.concat(getById) + "?idalumno=" + alumno.getId();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                volleyResponseListener.onResponse(response);
                Toast.makeText(context, "" + response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                estado = false;
            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(request);
        return consulta;
    }

    public boolean deleteStudent(Alumno alumno){
        String path = host.concat(delete);
        JSONObject  object = new JSONObject();
        try {
            object.put("idalumno",alumno.getId());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // response
                Log.e("Response", response.toString());
                estado = true;
            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error.
            }});
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(dr);
        return estado;
    }
}
