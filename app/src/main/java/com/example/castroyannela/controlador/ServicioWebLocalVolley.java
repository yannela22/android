package com.example.castroyannela.controlador;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.castroyannela.modelo.Alumno;

import org.json.JSONObject;

import java.util.Map;

public class ServicioWebLocalVolley  {
    Context context;

    public ServicioWebLocalVolley(Context context) {
        this.context = context;
    }

    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(JSONObject response);
    }

    String host = "http://10.20.7.119/CastroYannela";   //escribir la ip del internet cuando se trabaja con servidor local
    String insert = "/wsJSONRegistroMovil.php";
    String get = "/wsJSONConsultarLista.php";
    String getById = "/wsJSONConsultarUsuario.php";
    String update = "/wsJSONUpdateMovil.php";
    String delete = "/wsJSONDeleteMovil.php";
    String consulta;

    public String findAllUsers(final VolleyResponseListener volleyResponseListener){
        String path = host.concat(get);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                volleyResponseListener.onResponse(response);
                Toast.makeText(context, "" + response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(request);
        return consulta;
    }

    public String getUserById(Alumno alumno, final VolleyResponseListener volleyResponseListener) {
        String path = host.concat(getById) + "?documento=" + alumno.getId();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                volleyResponseListener.onResponse(response);
                Toast.makeText(context, "" + response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(request);
        return consulta;
    }
    public String deleteUser(Alumno alumno, final VolleyResponseListener volleyResponseListener){
        String path = host.concat(delete)+ "?documento=" + alumno.getId();
        StringRequest dr = new StringRequest(Request.Method.GET, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyResponseListener.onError(response);
                consulta = response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(dr);
        return consulta;
    }

    public String insertarStudents(final Map<String, String> parameters, final VolleyResponseListener listener){
        String path = host.concat(insert);


        //metod,url,json,eventos
        StringRequest request = new StringRequest(Request.Method.POST,path, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                listener.onError(response);
                consulta = response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return parameters;
            }
        };
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(request);
        return consulta;
    }

    public String updateUser(final Map<String, String> parameters, final VolleyResponseListener listener){
        String path = host.concat(update);


        //metod,url,json,eventos
        StringRequest request = new StringRequest(Request.Method.POST,path, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                listener.onError(response);
                consulta = response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return parameters;
            }
        };
        AlumnoSingletonVolly.getInstance(context).addToRequestQueue(request);
        return consulta;
    }

}
