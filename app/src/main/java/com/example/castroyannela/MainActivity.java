package com.example.castroyannela;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.castroyannela.vistas.actividades.ActividadEnviarParametro;
import com.example.castroyannela.vistas.actividades.ActividadEscucharFragmento;
import com.example.castroyannela.vistas.actividades.ActividadFragmento;
import com.example.castroyannela.vistas.actividades.ActividadRecyclerArtistas;
import com.example.castroyannela.vistas.actividades.ActivityLogin;
import com.example.castroyannela.vistas.actividades.ActivitySuma;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button botonLogin, botonSumar, botonParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cargarComponentes();
    }

    private void cargarComponentes(){
        botonLogin = findViewById(R.id.btnIrLogin);
        botonSumar = findViewById(R.id.btnIrSumar);
        botonParametro = findViewById(R.id.btnParametro);
        botonLogin.setOnClickListener(this);
        botonSumar.setOnClickListener(this);
        botonParametro.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()){
            case R.id.btnIrLogin:
                intent = new Intent(MainActivity.this, ActivityLogin.class);
                startActivity(intent);
                break;
            case R.id.btnIrSumar:
                intent = new Intent(MainActivity.this, ActivitySuma.class);
                startActivity(intent);
                break;
            case R.id.btnParametro:
                intent = new Intent(MainActivity.this, ActividadEnviarParametro.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //metodo para cargar menus
        //menuInflater permite crear un objeto para manejar el archivo xml(main.xml)
        //metodo inflate permite agregar un menu implementado de un archivo xml a la actividad
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //Este metodo permite realizar eventos en cada item hijo de los menus
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(MainActivity.this, ActivityLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionSumar:
                intent = new Intent(MainActivity.this, ActivitySuma.class);
                startActivity(intent);
                break;
            case R.id.opcionParametros:
                intent = new Intent(MainActivity.this, ActividadEnviarParametro.class);
                startActivity(intent);
                break;
            case R.id.opcionFrgColores:
                intent = new Intent(MainActivity.this, ActividadFragmento.class);
                startActivity(intent);
                break;
            case R.id.opcionFrgEscuchar:
                intent = new Intent(MainActivity.this, ActividadEscucharFragmento.class);
                startActivity(intent);
                break;
            case R.id.opcionDlgSumar:
                final Dialog dlgSumar = new Dialog(MainActivity.this);
                dlgSumar.setContentView(R.layout.dlg_sumar);
                final EditText cajaN1 = dlgSumar.findViewById(R.id.txtN1Dlg);
                final EditText cajaN2 = dlgSumar.findViewById(R.id.txtN2Dlg);
                Button botonSumarDlg = dlgSumar.findViewById(R.id.btnSumarDlg);
                botonSumarDlg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resultado = Integer.parseInt(cajaN1.getText().toString()) + Integer.parseInt(cajaN2.getText().toString());
                        Toast.makeText(MainActivity.this, "Suma = " + resultado, Toast.LENGTH_SHORT).show();
                        dlgSumar.hide();
                    }
                });
                dlgSumar.show();
                break;
            case R.id.opcionArtistas:
                intent = new Intent(MainActivity.this, ActividadRecyclerArtistas.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}
